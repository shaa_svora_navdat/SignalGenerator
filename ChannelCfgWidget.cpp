#include "ChannelCfgWidget.h"
#include "ui_ChannelCfgWidget.h"

#include <sdr_math.h>

ChannelCfgWidget::ChannelCfgWidget(int idx, QString Name, Frequency_t fd, Frequency_t bandwidth, Frequency_t freq, Sample_t gain, QWidget *parent) :
  QWidget(parent),
  ui(new Ui::ChannelCfgWidget)
{
  ui->setupUi(this);

  Idx = idx;

  setName(Name);
  setFreqParams(fd, bandwidth);
  setFreq(freq);
  setGain(gain);
}

ChannelCfgWidget::~ChannelCfgWidget()
{
  delete ui;
}

void ChannelCfgWidget::setFreqParams(Frequency_t fd, int bandwidth)
{
  int val = fd/2 - bandwidth/2;
  ui->Freq->setMinimum(-val);
  ui->Freq->setMaximum(val);
}

void ChannelCfgWidget::setName(QString name)
{
  ui->Cfgs->setTitle(name);
}

void ChannelCfgWidget::setFreq(Frequency_t freq)
{
  ui->Freq->setValue(freq);
  emit freq_changed(Idx, freq);
}

void ChannelCfgWidget::setGain_dB(Sample_t gain)
{
  ui->Gain->setValue(gain);
  emit gain_changed(Idx, from_dB(gain));
}

void ChannelCfgWidget::setGain(Sample_t gain)
{
  setGain_dB(to_dB(gain));
}

void ChannelCfgWidget::on_Freq_valueChanged(double arg1)
{
  emit freq_changed(Idx, arg1);
}

void ChannelCfgWidget::on_Gain_valueChanged(double arg1)
{
  emit gain_changed(Idx, from_dB(arg1));
}
