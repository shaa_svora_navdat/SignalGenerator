#-------------------------------------------------
#
# Project created by QtCreator 2016-05-15T14:59:12
#
#-------------------------------------------------

QT       += core gui multimedia svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = NavdatSignalGenerator
TEMPLATE = app

DEFINES += "NAVDAT_FFT_SIZE_FACTOR=4"

SOURCES += main.cpp\
        MainWindow.cpp \
    qSDR/QSdrTransmitter.cpp \
    qSDR/QSdrNavdatTransmitter.cpp \
    qSDR/QSdrOutput.cpp \
    qSDR/QSdrTestTransmitter.cpp \
    ChannelCfgWidget.cpp \
    qSDR/QSdrAudioOutput.cpp \
    qSDR/QSdrUdpOutput.cpp \
    qSDR/QSdrOutputDriver.cpp \
    qSDR/QSdrChannelsMultiplexer.cpp \
    LedIndicator.cpp \
    QLed/qledindicator.cpp \
    qSDR/QSdrNavtexTransmitter.cpp

HEADERS  += MainWindow.h \
    qSDR/QSdrTransmitter.h \
    qSDR/QSdrNavdatTransmitter.h \
    qSDR/QSdrOutput.h \
    qSDR/QSdrTestTransmitter.h \
    ChannelCfgWidget.h \
    qSDR/QSdrAudioOutput.h \
    qSDR/QSdrUdpOutput.h \
    qSDR/QSdrOutputDriver.h \
    qSDR/QSdrChannelsMultiplexer.h \
    LedIndicator.h \
    QLed/qledindicator.h \
    QLeadingZerosSpinBox.h \
    qSDR/QSdrNavtexTransmitter.h

FORMS    += MainWindow.ui \
    ChannelCfgWidget.ui \
    LedIndicator.ui

include(qSDR/SDR/sdr.pri)
INCLUDEPATH += $$PWD/qSDR $$PWD/QLed

CONFIG (debug, debug|release) {
 DESTDIR = $$PWD/../build/debug
} else {
 DESTDIR = $$PWD/../build/release
}

unix {
    target.path = /usr/local/bin
    INSTALLS += target
}
