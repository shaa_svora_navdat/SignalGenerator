#include "QSdrOutput.h"

QSdrOutput::QSdrOutput(QObject *parent) : QObject(parent)
{
  reset();
}

QSdrOutput::~QSdrOutput()
{
  reset();
}

void QSdrOutput::stopWhenReady()
{
  NeedForStop = true;
}
