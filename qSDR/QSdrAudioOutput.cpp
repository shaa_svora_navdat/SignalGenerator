#include "QSdrAudioOutput.h"
#include <QDataStream>

#include <qendian.h>

QSdrAudioOutput::QSdrAudioOutput(Frequency_t Fd, QObject *parent) :
  QSdrOutput(parent)
{
  audio = 0;
  audioIO = 0;

  audioFormat.setSampleRate(Fd);
  audioFormat.setChannelCount(2);
  audioFormat.setSampleType(QAudioFormat::SignedInt);
  audioFormat.setSampleSize(16);
  audioFormat.setCodec("audio/pcm");
  audioFormat.setByteOrder(QAudioFormat::LittleEndian);

  sample_size = audioFormat.sampleSize()/8;
}

bool QSdrAudioOutput::setDevice(QAudioDeviceInfo DeviceInfo)
{
  audioDevice = DeviceInfo; 
  return audioDevice.isFormatSupported(audioFormat);
}

void QSdrAudioOutput::create()
{
  remove();

  audio = new QAudioOutput(audioDevice, audioFormat);
  audioCollector.clear();
  audio->moveToThread(thread());

#ifdef QSDR_AUDIO_OUTPUT_USE_TIMER
  Timer = new QTimer();
  Timer->moveToThread(thread());
  connect(Timer, SIGNAL(timeout()), this, SLOT(processor()));
#endif

  audioIO = audio->start();
  connect(audio, SIGNAL(stateChanged(QAudio::State)), this, SLOT(audio_state_handler(QAudio::State)));
#ifdef QSDR_AUDIO_OUTPUT_USE_TIMER
  Timer->start(audioFormat.durationForBytes(2*audio->periodSize())/1000);
#else
  qDebug("QSdrAudioOutput: BufSize = %d; NotifyInterval = %d", audio->bufferSize(), audio->notifyInterval());
  connect(audio, SIGNAL(notify()), this, SLOT(processor()));
  processor();
#endif
  return;
}

void QSdrAudioOutput::remove()
{
  if (audio)
  {
    disconnect(audio, SIGNAL(stateChanged(QAudio::State)), this, SLOT(audio_state_handler(QAudio::State)));
#ifdef QSDR_AUDIO_OUTPUT_USE_TIMER
    disconnect(Timer, SIGNAL(timeout()), this, SLOT(processor()));
    Timer->stop();
    Timer->deleteLater();
#else
    disconnect(audio, SIGNAL(notify()), this, SLOT(processor()));
#endif

    audioCollector.clear();
    audioIO = 0;
    audio->deleteLater();
    audio = 0;
  }
}

void QSdrAudioOutput::write_samples(iqSample_t* samples, Size_t count)
{
  QByteArray buf;
  buf.resize(2*count*sample_size);
  unsigned char* ptr = reinterpret_cast<unsigned char*>(buf.data());
  for (Size_t i = 0; i < count; i++)
  {
    convert_sample(samples[i].i, ptr);
    convert_sample(samples[i].q, ptr);
  }
  audioCollector.append(buf);
}

void QSdrAudioOutput::convert_sample(Sample_t sample, unsigned char*& ptr)
{
  if (isnan(sample))
    sample = 0;
  if (ABS(sample) > max_value)
    sample = SIGN(sample)*max_value;

  Sample_t X;
  if (sample)
    X = sample*Gain*32767;
  else
    X = 0;

  SDR_ASSERT(X <= 32767);
  qint16 x = static_cast<qint16>(X);

  qToLittleEndian<qint16>(x, ptr);
  ptr += sample_size;
}

void QSdrAudioOutput::audio_state_handler(QAudio::State state)
{
  switch (state)
  {
  case QAudio::IdleState:
    if (NeedForStop && audioCollector.isEmpty())
      emit readyForStop();
    break;

  default:
    break;
  }
}

void QSdrAudioOutput::processor()
{
  if (NeedForStop && audioCollector.isEmpty())
    return;

  int count = audio->bytesFree()/audio->periodSize();
  while (count--)
  {
    while(audioCollector.size() < audio->periodSize())
    {
      if (!NeedForStop)
        emit bufferReady();
      else
        audioCollector.append((char)0);
    }

    int realWriteSize = audioIO->write(audioCollector.data(), audio->periodSize());
    audioCollector.remove(0, realWriteSize);
  }
}
