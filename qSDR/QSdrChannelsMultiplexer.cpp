#include "QSdrChannelsMultiplexer.h"

#include "sdr_math.h"
#include "sdr_complex.h"

QSdrChannelsMultiplexer::QSdrChannelsMultiplexer(Frequency_t Fd, int SamplesCountForBuf, QObject *parent) :
  QObject(parent)
{
  this->Fd = Fd;
  SamplesCount = SamplesCountForBuf;

  procBuf.resize(SamplesCount);
  ucProcBuf.resize(SamplesCount);
  muxBuf.resize(SamplesCount);

  Noise_AwgnCfg_t cfgAwgn;
  cfgAwgn.SignalLevel = 1;
  cfgAwgn.SignalNoiseRatio = 80;
  init_Noise_Awgn(&Awgn, &cfgAwgn);
}

QSdrChannelsMultiplexer::~QSdrChannelsMultiplexer()
{
  foreach (UpConverter_t* p, UpConverters)
    if (p)
      delete p;
}

bool QSdrChannelsMultiplexer::isEnabledChannels()
{
  foreach(QSdrTransmitter* tr, Transmitters)
    if (tr->isEnabled())
      return true;
  return false;
}

int QSdrChannelsMultiplexer::addChannel(Frequency_t freq, QSdrTransmitter* transmitter, Sample_t gain)
{
  int idx = Transmitters.count();
  Transmitters.append(transmitter);
  transmitter->moveToThread(thread());

  Gains.append(gain);

  UpConverters.append(0);
  setChannelFreq (idx, freq);

  return idx;
}

void QSdrChannelsMultiplexer::setChannelFreq(int index, Frequency_t freq)
{
  UpConverter_t* upConverter = UpConverters[index];
  if (freq)
  {
    if (!upConverter)
      upConverter = new UpConverter_t;

    UpConverterCfg_t cfgUC;
    cfgUC.Fd = Fd;
    cfgUC.F = freq;
    init_UpConverter(upConverter, &cfgUC);
  }
  else if (upConverter)
  {
    delete upConverter;
    upConverter = 0;
  }
  UpConverters[index] = upConverter;
}

void QSdrChannelsMultiplexer::setChannelGain(int index, Sample_t gain)
{
  Gains[index] = gain;
}

void QSdrChannelsMultiplexer::setAwgnNoiseLevel_dB(double level)
{
  Noise_Awgn_setLevel_dB(&Awgn, level);
}

void QSdrChannelsMultiplexer::stream_processor()
{
  clear_buffer_iq(muxBuf.data(), SamplesCount);

  for (int i = 0; i < Transmitters.count(); i++)
  {
    if (!Transmitters[i]->isEnabled()) continue;

    iqSample_t* Samples = procBuf.data();

    Transmitters[i]->stream_out(Samples, SamplesCount);

    if (Gains[i] != 1)
      gain_iq(Gains[i], Samples, SamplesCount, Samples);

    if (UpConverters[i])
    {
      up_converter_iq(UpConverters[i], Samples, SamplesCount, ucProcBuf.data());
      Samples = ucProcBuf.data();
    }

    for (Size_t j = 0; j < SamplesCount; j++)
      muxBuf.data()[j] = iq_sum(muxBuf.data()[j], Samples[j]);
  }
  noise_awgn_iq(&Awgn, muxBuf.data(), SamplesCount, muxBuf.data());
  emit samplesReady(muxBuf.data(), SamplesCount);
}
