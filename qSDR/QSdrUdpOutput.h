#ifndef QSDRUDPOUTPUT_H
#define QSDRUDPOUTPUT_H

#include <QSdrOutput.h>
#include <QUdpSocket>
#include <QTimer>

class QSdrUdpOutput : public QSdrOutput
{
  Q_OBJECT
public:
  explicit QSdrUdpOutput(Frequency_t Fd, int SamplesCountForBuf, QObject *parent = 0);

  bool setIpConfig(QHostAddress Addr, quint16 Port);

signals:

public slots:
  void create();
  void remove();
  void write_samples(iqSample_t* samples, Size_t count);

private:
  qint64 Timeout_us;
  QTimer *Timer;

  QUdpSocket* udp;
  QHostAddress udpAddr;
  quint16 udpPort;
  quint16 udpSeqNum;

private slots:
  void processor();
};

#endif // QSDRUDPOUTPUT_H
