#ifndef QSDROUTPUT_H
#define QSDROUTPUT_H

#include <QObject>
#include <sdr_basic.h>
#include <sdr_math.h>

class QSdrOutput : public QObject
{
  Q_OBJECT
public:
  explicit QSdrOutput(QObject *parent = 0);
  ~QSdrOutput();

signals:
  void bufferReady();
  void readyForStop();

public slots:  
  virtual void create() = 0;
  virtual void remove() = 0;
  virtual void reset(){NeedForStop = false;}

  virtual void stopWhenReady();

  virtual void write_samples(iqSample_t* samples, Size_t count) = 0;

  void setGain_dB(double gain){Gain = from_dB(gain); refresh_gain(Gain);}

protected:
  bool NeedForStop;
  Sample_t Gain;

  virtual void refresh_gain(Sample_t gain){Q_UNUSED(gain)}
};

#endif // QSDROUTPUT_H
