#include "QSdrNavdatTransmitter.h"
#include <QDateTime>
#include <QSettings>

extern "C"
{
  static void fill_empty_buf(QSdrNavdatTransmitter* This, iqSample_t* Samples, Size_t Count)
  {
    This->fill_empty_buf_handler(Samples, Count);
  }
  static void ifft_samples_sink(QSdrNavdatTransmitter* This, iqSample_t* Samples, Size_t Count)
  {
    This->ifft_samples_handler(Samples, Count);
  }
  static void data_sent(QSdrNavdatTransmitter* This)
  {
    This->data_sent_handler();
  }
}

QSdrNavdatTransmitter::QSdrNavdatTransmitter(Frequency_t Fd, QObject* parent) :
  QSdrTransmitter(Fd, parent)
{
  qRegisterMetaType<NAVDAT_TIS_qamMode_t>("NAVDAT_TIS_qamMode_t");
  qRegisterMetaType<NAVDAT_DS_qamMode_t>("NAVDAT_DS_qamMode_t");
  qRegisterMetaType<NAVDAT_DS_Code_t>("NAVDAT_DS_Code_t");

  counterFileId = 0;
  currentFrameData.buf = 0;

  /* Буфера */

  Data.resize(NAVDAT_dsRawCountMax(NAVDAT_DS_qam64));

  misDataBuf.resize(NAVDAT_MIS_CELLS_COUNT);
  tisDataBuf.resize(NAVDAT_TIS_CELLS_COUNT);
  dsDataBuf.resize(NAVDAT_DS_CELLS_COUNT);

  /* Интерфейсы к буферам */

  rawData.mis.P = misDataBuf.data();
  rawData.mis.Size = misDataBuf.size();
  rawData.tis.P = tisDataBuf.data();
  rawData.tis.Size = tisDataBuf.size();
  rawData.ds.P = dsDataBuf.data();
  rawData.ds.Size = dsDataBuf.size();

  /* Инициализация */

  NAVDAT_ModulatorCfg_t cfgMod;
  cfgMod.fftSizeFactor = NAVDAT_FFT_SIZE_FACTOR;
  cfgMod.fftAmplitudeFactor = NAVDAT_FFT_A_FACTOR;
  cfgMod.qamTIS = NAVDAT_TIS_qam4;
  cfgMod.qamDS = NAVDAT_DS_qam4;
  init_NAVDAT_Modulator(&Mod, &cfgMod);

  /* Обратные вызовы */

  NAVDAT_ModulatorCallback_t callbackMod;
  callbackMod.on_data_sent = (event_sink_t)data_sent;
  callbackMod.fill_empty_buf = (samples_sink_iq_t)fill_empty_buf;
  callbackMod.ifft_samples_sink = (samples_sink_iq_t)ifft_samples_sink;
  NAVDAT_Modulator_set_callback(&Mod, this, &callbackMod);
}

QSdrNavdatTransmitter::~QSdrNavdatTransmitter()
{
  reset();
}

void QSdrNavdatTransmitter::reset()
{  
  NAVDAT_Modulator_reset(&Mod);

  filesMetaData.clear();
  while (framesData.count())
  {
    FrameData data = framesData.takeFirst();
    delete data.buf;
  }

  currentFileMetaData.FramesCount = 0;
  currentFileMetaData.Name.clear();
  if (currentFrameData.buf)
  {
    delete currentFrameData.buf;
    currentFrameData.buf = 0;
  }

  if (isTransmit())
    setIsTransmit(0);
}

void QSdrNavdatTransmitter::write_frame_to_modulator(NAVDAT_RawFrame_t* Frame)
{
  rawData.mis.Size = misDataBuf.size();
  rawData.tis.Size = tisDataBuf.size();
  rawData.ds.Size = dsDataBuf.size();

  Bool_t res;
  res = NAVDAT_FrameToData(Frame, &rawData);
  SDR_ASSERT(res);
  res = NAVDAT_Modulator_transmit(&Mod, &rawData);
  SDR_ASSERT(res);
}

bool QSdrNavdatTransmitter::write_next_frame_to_modulator()
{
  if (framesData.size())
  {
    currentFrameData = framesData.takeFirst();
    if (currentFrameData.isFirst)
      currentFileMetaData = filesMetaData.takeFirst();
    write_frame_to_modulator(currentFrameData.buf);
    delete currentFrameData.buf;
    currentFrameData.buf = 0;
    return 1;
  }
  return 0;
}

void QSdrNavdatTransmitter::fill_empty_buf_handler(iqSample_t *Samples, Size_t Count)
{
  if (!write_next_frame_to_modulator())
  {
    if (isTransmit())
    {
      setIsTransmit(0);
    }
    clear_buffer_iq(Samples, Count);
  }
}

void QSdrNavdatTransmitter::ifft_samples_handler(iqSample_t *Samples, Size_t Count)
{
  Q_UNUSED(Samples);
  Q_UNUSED(Count);
}

void QSdrNavdatTransmitter::data_sent_handler()
{
  emit frame_transmitted(currentFrameData.idFile, currentFrameData.id, currentFrameData.isFirst, currentFrameData.isLast);
  write_next_frame_to_modulator();
}

Size_t QSdrNavdatTransmitter::maxDataInPacket(NAVDAT_MultiplexCfg_t cfgMux)
{
  NAVDAT_Multiplex_t Mux;
  init_NAVDAT_Multiplex(&Mux, &cfgMux);
  return NAVDAT_Multiplex_dsDataCountMax(&Mux);
}

#include <QDataStream>

template<typename T>
static void settings_setArray(QSettings &settings, QString key, T* data, quint32 size)
{
  QByteArray array;
  QDataStream in(&array, QIODevice::WriteOnly);
  in.setVersion(QDataStream::Qt_5_5);
  for (quint32 i = 0; i < size; ++i)
      in << data[i];
  settings.setValue(key, array);
}

void QSdrNavdatTransmitter::saveToFile(NAVDAT_MultiplexCfg_t cfgMux, QString path, QString fileName, QByteArray fileData, quint32 mmsi, qint16 fileId, quint16 timestamp)
{
  if (fileId == -1)
    fileId = counterFileId;

  QSettings settings(QString("%1/%2.navdat").arg(path).arg(fileName), QSettings::IniFormat);
  settings.setValue("tis_qam_mode", cfgMux.modeTIS);
  settings.setValue("ds_qam_mode", cfgMux.modeDS);
  settings.setValue("xis_code", cfgMux.codeXIS);
  settings.setValue("ds_code", cfgMux.codeDS);

  NAVDAT_RawFrame_t Frame, encFrame;

  FileNameBuf = fileName.toLocal8Bit();
  init_converter(cfgMux);

  UInt16_t id = 0;
  int dataCounter = 0, dataSize = fileData.size();
  Bool_t isFirst = 1;
  do
  {
    settings.beginGroup(QString("frame_%1").arg(id));

    Size_t restDataSize = dataSize - dataCounter;
    Size_t size = prepare_frame(mmsi, fileId, id++, timestamp, isFirst, restDataSize);
    if (isFirst) isFirst = 0;
    convert_packet((UInt8_t*)&fileData.data()[dataCounter], size, &Frame, &encFrame);
    dataCounter += size;

    settings_setArray<quint8>(settings, "mis_data", Frame.misData, Frame.Count.mis);
    settings_setArray<quint8>(settings, "tis_data", Frame.tisData, Frame.Count.tis);
    settings_setArray<quint8>(settings,  "ds_data", Frame.dsData,  Frame.Count.ds);

    settings_setArray<quint8>(settings, "mis_enc_data", encFrame.misData, encFrame.Count.mis);
    settings_setArray<quint8>(settings, "tis_enc_data", encFrame.tisData, encFrame.Count.tis);
    settings_setArray<quint8>(settings,  "ds_enc_data", encFrame.dsData,  encFrame.Count.ds);

    NAVDAT_FrameToData(&encFrame, &rawData);

    settings_setArray<qint8>(settings, "mis_qam_data", rawData.mis.P, rawData.mis.Size);
    settings_setArray<qint8>(settings, "tis_qam_data", rawData.tis.P, rawData.tis.Size);
    settings_setArray<qint8>(settings,  "ds_qam_data", rawData.ds.P,  rawData.ds.Size);

    settings.endGroup();
  } while (dataCounter < dataSize);
  settings.setValue("frame_count", id);

  deinit_converter();
}

void QSdrNavdatTransmitter::transmit(NAVDAT_MultiplexCfg_t cfgMux, QString fileName, QByteArray fileData, quint32 mmsi, qint16 fileId, quint16 timestamp)
{
  init_converter(cfgMux);
  convert_file(mmsi, fileName, fileData, fileId, timestamp);
  deinit_converter();

  setIsTransmit(1);
}

void QSdrNavdatTransmitter::init_converter(NAVDAT_MultiplexCfg_t cfgMux)
{
  init_NAVDAT_Multiplex(&Mux, &cfgMux);
  NAVDAT_CodecCfg_t cfgCod;
  cfgCod.tisMode = cfgMux.modeTIS;
  cfgCod.dsMode = cfgMux.modeDS;
  cfgCod.xisCode = cfgMux.codeXIS;
  cfgCod.dsCode = cfgMux.codeDS;
  init_NAVDAT_Codec(&Cod, &cfgCod);

  QByteArray SenderIdBuf = SenderID.toLocal8Bit();
  NAVDAT_Multiplex_setSenderID(&Mux, SenderIdBuf.data());
}

void QSdrNavdatTransmitter::deinit_converter()
{
  deinit_NAVDAT_Codec(&Cod);
}

void QSdrNavdatTransmitter::convert_file(quint32 mmsi, QString fileName, QByteArray fileData, qint16 fileId, quint16 timestamp)
{
  FileMetaData fileMetaData;
  QList<FrameData> fileFramesData;
  fileMetaData.Name = fileName;

  if (fileId == -1)
    fileId = counterFileId++;
  else
    counterFileId = fileId + 1;

  FileNameBuf = fileName.toLocal8Bit();

  UInt16_t id = 0;
  int FramesCounter = 0;
  int dataCounter = 0, dataSize = fileData.size();
  Bool_t isFirst = 1;
  do
  {
    FrameData frameData;
    frameData.idFile = fileId;
    frameData.id = id;

    Size_t restDataSize = dataSize - dataCounter;
    Size_t size = prepare_frame(mmsi, fileId, id++, timestamp, isFirst, restDataSize);
    if (isFirst) isFirst = 0;

    frameData.isFirst = NAVDAT_frameIsFirst(&converterFrame);
    frameData.isLast = NAVDAT_frameIsLast(&converterFrame);

    NAVDAT_RawFrame_t Frame, *encFrame = new NAVDAT_RawFrame_t;
    Q_ASSERT(encFrame);
    convert_packet((UInt8_t*)&fileData.data()[dataCounter], size, &Frame, encFrame);
    dataCounter += size;

    frameData.buf = encFrame;
    fileFramesData.append(frameData);
    ++FramesCounter;
  } while (dataCounter < dataSize);

  fileMetaData.FramesCount = FramesCounter;
  filesMetaData.append(fileMetaData);
  framesData.append(fileFramesData);
}

Size_t QSdrNavdatTransmitter::prepare_frame(quint32 mmsi, quint8 FileId, UInt16_t id, quint16 timestamp, bool isFirst, Size_t restDataSize)
{
  long size;

  converterFrame.idFile = FileId;
  converterFrame.mmsi = mmsi;
  converterFrame.id = id;
  converterFrame.flags = 0;
  if (isFirst)
  {
    converterFrame.flags = 1;
    converterFrame.FileName = FileNameBuf.data();
  }
  else
    converterFrame.FileName = 0;
  size = NAVDAT_Multiplex_dsDataCountMax(&Mux);
  if (size >= restDataSize)
  {
    size = restDataSize;
    converterFrame.flags |= 2;
  }

  QDateTime DateTime = QDateTime::currentDateTimeUtc();
  NAVDAT_Multiplex_setDate(&Mux, DateTime.date().year()%100, DateTime.date().month(), DateTime.date().day());
  NAVDAT_Multiplex_setTime(&Mux, DateTime.time().hour(), DateTime.time().minute(), DateTime.time().second());
  if (timestamp == 0)
    timestamp = DateTime.time().msec();
  converterFrame.timestamp = timestamp;

  return size;
}

void QSdrNavdatTransmitter::convert_packet(UInt8_t* data, Size_t size, NAVDAT_RawFrame_t *Frame, NAVDAT_RawFrame_t *encFrame)
{
  converterFrame.data.P = data;
  converterFrame.data.Size = size;

  if (!navdat_multiplex_write(&Mux, &converterFrame, Frame))
    qWarning("NAVDAT: transmit: multiplex failure");
  navdat_encode(&Cod, Frame, encFrame);
}

void QSdrNavdatTransmitter::stream_out(iqSample_t *samples, Size_t count)
{
  navdat_modulator_iq(&Mod, samples, count);
}
