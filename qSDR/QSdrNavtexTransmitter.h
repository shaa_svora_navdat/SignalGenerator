#ifndef QSDRNAVTEXTRANSMITTER_H
#define QSDRNAVTEXTRANSMITTER_H

#include "QSdrTransmitter.h"

#include <SDR/NAVTEX/Transmitter.h>

#include <QList>
#include <QVector>

class QSdrNavtexTransmitter : public QSdrTransmitter
{
  Q_OBJECT

public:
  explicit QSdrNavtexTransmitter(Frequency_t Fd, QObject* parent = 0);
  ~QSdrNavtexTransmitter();

  void send_message(char senderId, char type, Int8_t id, QString msg);

  void buffer_ready_handler();
  void transmit_finished_handler();

public slots:
  void reset();

signals:
  void char_transmitted(int msg_id, int char_id);

private:
  Size_t startSeq_Count, finishSeq_Count;

  NAVTEX_Transmitter_t Trmtr;

  QList<Int8_t> DataQueue;

  QVector<Sample_t> grtrBuf;
  QVector<Int8_t> trmtrBuf;

  void stream_out(iqSample_t* samples, Size_t count);
};

#endif // QSDRNAVTEXTRANSMITTER_H
