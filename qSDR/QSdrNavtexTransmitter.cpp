#include "QSdrNavtexTransmitter.h"

#include <SDR/NAVTEX/Codec/ita2.h>

extern "C"
{
  void trmtr_buffer_ready_handler(QSdrNavtexTransmitter* This)
  {
    This->buffer_ready_handler();
  }

  void trmtr_transmit_finished_handler(QSdrNavtexTransmitter* This)
  {
    This->transmit_finished_handler();
  }
}

QSdrNavtexTransmitter::QSdrNavtexTransmitter(Frequency_t Fd, QObject* parent) :
  QSdrTransmitter(Fd, parent)
{
  startSeq_Count = 8;
  finishSeq_Count = 8;

  grtrBuf.resize(Fd);
  trmtrBuf.resize(128);

  NAVTEX_TransmitterCfg_t cfg;
  cfg.Fd = Fd;
  cfg.BufferReadyTriggerLevel = 0;
  cfg.generatorBuf.P = grtrBuf.data();
  cfg.generatorBuf.Size = grtrBuf.size();
  cfg.Buffer.P = trmtrBuf.data();
  cfg.Buffer.Size = trmtrBuf.size();
  init_NAVTEX_Transmitter(&Trmtr, &cfg);
  NAVTEX_TransmitterCallback_t callback;
  callback.on_buffer_ready = (event_sink_t)trmtr_buffer_ready_handler;
  callback.on_transmit_finished = (event_sink_t)trmtr_transmit_finished_handler;
  NAVTEX_Transmitter_set_callback(&Trmtr, this, &callback);
}

QSdrNavtexTransmitter::~QSdrNavtexTransmitter()
{
  reset();
}

void QSdrNavtexTransmitter::send_message(char senderId, char type, Int8_t id, QString msg)
{
  QString header = QString("\r\nZCZC %1%2%3\r\n").arg(senderId).arg(type).arg(id);

  QByteArray data;
  data.append(header);
  data.append(msg.toUpper().toLocal8Bit());
  data.append("\r\nNNNN\r\n");

  QList<Int8_t> ita2Data;

  for (Size_t i = 0; i < startSeq_Count; ++i)
    ita2Data.append(NAVTEX_SEQUENCE_SYNC);

  bool isNumeric = false;
  for(int i = 0; i < data.size(); ++i)
  {
    if (isNumeric)
    {
      if (ita2_switch_alphabetic(data[i]))
      {
        isNumeric = false;
        ita2Data.append(ita2_encode(ITA2_ASCII_ALPHABETIC));
      }
    }
    else
    {
      if (ita2_switch_numeric(data[i]))
      {
        isNumeric = true;
        ita2Data.append(ita2_encode(ITA2_ASCII_NUMERIC));
      }
    }
    ita2Data.append(ita2_encode(data[i]));
  }

  for (Size_t i = 0; i < finishSeq_Count; ++i)
    ita2Data.append(NAVTEX_SEQUENCE_END);

  DataQueue.append(ita2Data);

  setIsTransmit(1);
}

void QSdrNavtexTransmitter::buffer_ready_handler()
{
  if (!isTransmit()) return;

  bool isFull = false;
  while (!isFull)
  {
    if (DataQueue.isEmpty())
      return;

    Int8_t x = DataQueue.first();
    if (navtex_transmit_1(&Trmtr, x))
      DataQueue.removeFirst();
    else
      isFull = true;
  }
}

void QSdrNavtexTransmitter::transmit_finished_handler()
{
  setIsTransmit(0);
}

void QSdrNavtexTransmitter::reset()
{
  DataQueue.clear();

  if (isTransmit())
    setIsTransmit(0);
}

void QSdrNavtexTransmitter::stream_out(iqSample_t* samples, Size_t count)
{
  navtex_transmitter(&Trmtr, samples, count);
}
