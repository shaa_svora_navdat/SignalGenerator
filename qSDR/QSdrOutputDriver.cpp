#include "QSdrOutputDriver.h"

QSdrOutputDriver::QSdrOutputDriver(Frequency_t Fd, int SamplesCountForBuf, QObject *parent) :
  QObject(parent), Mux(Fd, SamplesCountForBuf)
{
  Mux.moveToThread(&Thread);
  Output = 0;
  flag_Worked = false;
}

void QSdrOutputDriver::setOutput(QSdrOutput* Output)
{
  if (Thread.isRunning())
  {
    qWarning("OUTPUT: Driver: cann't set current output when running");
    return;
  }

  if (this->Output)
    disableOutput(this->Output);
  this->Output = Output;

  if (Output->thread() != &Thread)
    Output->moveToThread(&Thread);
  enableOutput(Output);
}

void QSdrOutputDriver::enableOutput(QSdrOutput* Output)
{
  connect(&Thread, SIGNAL(started()), Output, SLOT(create()));
  connect(&Thread, SIGNAL(finished()), Output, SLOT(remove()));

  connect(Output, SIGNAL(bufferReady()), &Mux, SLOT(stream_processor()));
  connect(Output, SIGNAL(readyForStop()), this, SLOT(stop()));
  connect(&Mux, SIGNAL(samplesReady(iqSample_t*,Size_t)), Output, SLOT(write_samples(iqSample_t*,Size_t)));
}

void QSdrOutputDriver::disableOutput(QSdrOutput* Output)
{
  disconnect(&Thread, SIGNAL(started()), Output, SLOT(create()));
  disconnect(&Thread, SIGNAL(finished()), Output, SLOT(remove()));

  disconnect(Output, SIGNAL(bufferReady()), &Mux, SLOT(stream_processor()));
  disconnect(&Mux, SIGNAL(samplesReady(iqSample_t*,Size_t)), Output, SLOT(write_samples(iqSample_t*,Size_t)));
}

void QSdrOutputDriver::start()
{
  emit started();
  Thread.start();
  flag_Worked = true;
}

void QSdrOutputDriver::stop()
{
  if (!isRunning()) return;

  flag_Worked = false;
  Output->reset();
  Thread.quit();
  emit stopped();
  Thread.wait();
}

void QSdrOutputDriver::stopIfNeeded()
{
  if (!flag_Worked) return;
  if (Multiplexer()->isEnabledChannels()) return;

  Output->stopWhenReady();
}
