#ifndef QLEADINGZEROSSPINBOX_H
#define QLEADINGZEROSSPINBOX_H

#include <QSpinBox>

class QLeadingZerosSpinBox : public QSpinBox
{
  Q_OBJECT
public:
  QLeadingZerosSpinBox(QWidget * parent = 0) :
    QSpinBox(parent){}

  virtual QString textFromValue ( int value ) const
  {
    return QString("%1").arg(value, QString::number(maximum()).count(), 10, QChar('0'));
  }
};

#endif // QLEADINGZEROSSPINBOX_H
