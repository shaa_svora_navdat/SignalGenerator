#include "LedIndicator.h"
#include "ui_LedIndicator.h"

LedIndicator::LedIndicator(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::LedIndicator)
{
  ui->setupUi(this);

  ui->OutputLed->setOffColor1(Qt::gray);
  ui->OutputLed->setOffColor2(Qt::darkGray);
  ui->OutputLed->setOnColor1(QColor(255, 134, 0));
  ui->OutputLed->setOnColor2(QColor(127, 67, 0));

  ui->NavdatLed->setOffColor1(Qt::gray);
  ui->NavdatLed->setOffColor2(Qt::darkGray);
  ui->NavdatLed->setOnColor1(QColor(63, 255, 0));
  ui->NavdatLed->setOnColor2(QColor(31, 127, 0));

  ui->NavtexLed->setOffColor1(Qt::gray);
  ui->NavtexLed->setOffColor2(Qt::darkGray);
  ui->NavtexLed->setOnColor1(QColor(127, 120, 255));
  ui->NavtexLed->setOnColor2(QColor(63, 60, 127));
}

LedIndicator::~LedIndicator()
{
  delete ui;
}

void LedIndicator::setOutputRunningState()
{
  ui->label_Output->setStyleSheet(genStyleSheetWithColor(ui->OutputLed->getOnColor1()));
  ui->OutputLed->setChecked(true);
}

void LedIndicator::setOutputStoppedState()
{
  ui->label_Output->setStyleSheet(genStyleSheetWithColor(ui->OutputLed->getOffColor1()));
  ui->OutputLed->setChecked(false);
}

void LedIndicator::setNavdatTransmittingState()
{
  ui->label_Navdat->setStyleSheet(genStyleSheetWithColor(ui->NavdatLed->getOnColor1()));
  ui->NavdatLed->setChecked(true);
}

void LedIndicator::setNavdatStoppedState()
{
  ui->label_Navdat->setStyleSheet(genStyleSheetWithColor(ui->NavdatLed->getOffColor1()));
  ui->NavdatLed->setChecked(false);
}

void LedIndicator::setNavtexTransmittingState()
{
  ui->label_Navtex->setStyleSheet(genStyleSheetWithColor(ui->NavtexLed->getOnColor1()));
  ui->NavtexLed->setChecked(true);
}

void LedIndicator::setNavtexStoppedState()
{
  ui->label_Navtex->setStyleSheet(genStyleSheetWithColor(ui->NavtexLed->getOffColor1()));
  ui->NavtexLed->setChecked(false);
}

QString LedIndicator::genStyleSheetWithColor(QColor color)
{
  return QString("color:rgb(%1,%2,%3)").arg(color.red()).arg(color.green()).arg(color.blue());
}
